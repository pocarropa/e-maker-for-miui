@echo off
cd %~dp0
SETLOCAL ENABLEDELAYEDEXPANSION
TITLE M.E.K. - Miui Epic Kitchen Global for Xiaomi arm64 devices [by JamFlux]
SET CECHO=Tools\cecho.exe

cd /d "%~dp0"
goto admin_

:start
FOR %%k IN (*.zip) DO (SET ZIP=%%k)
SET TEMPPROYECTO=%ZIP:.zip=%
SET PROYECTO=%TEMPPROYECTO: =%
SET busybox=Tools\busybox
IF EXIST *.txt DEL *.txt
IF EXIST *.img DEL *.img
IF EXIST 1-Epic RMDIR /Q /S 1-Epic
IF EXIST 1-Project RMDIR /Q /S 1-Project
IF EXIST Tools\*.pyc DEL Tools\*.pyc
IF EXIST Tools\patches\updater-maker\original_symlinks DEL Tools\patches\updater-maker\original_symlinks
IF EXIST Tools\patches\updater-maker\kernel_file_contexts DEL Tools\patches\updater-maker\kernel_file_contexts
IF EXIST Tools\patches\updater-maker\not_recursive DEL Tools\patches\updater-maker\not_recursive
IF EXIST Tools\patches\updater-maker\recursive DEL Tools\patches\updater-maker\recursive
IF EXIST Tools\patches\updater-maker\rom_permissions DEL Tools\patches\updater-maker\rom_permissions
IF EXIST Tools\patches\updater-maker\system_contexts DEL Tools\patches\updater-maker\system_contexts
IF EXIST Tools\3-Deodexer\logs RMDIR /Q /S Tools\3-Deodexer\logs
SET TIMENOW=%TIME%

:Logo
cls
echo.
echo.
echo                                               `7MM
echo                                                 MM                      
echo        .gP"Ya      `7MMpMMMb.pMMMb.   ,6"Yb.    MM  ,MP'.gP"Ya `7Mb,od8 
echo       ,M'   Yb       MM    MM    MM  8)   MM    MM ;Y  ,M'   Yb  MM' "' 
echo       8M"""""" mmmmm MM    MM    MM   ,pm9MM    MM;Mm  8M""""""  MM     
echo       YM.    ,       MM    MM    MM  8M   MM    MM `Mb.YM.    ,  MM     
echo        `Mbmmd'     .JMML  JMML  JMML.`Moo9^Yo..JMML. YA.`Mbmmd'.JMML.
echo       ***************************************************************
%CECHO%                                                            {0b}by JamFlux{#}
echo.
TIMEOUT /T 3 /nobreak > NUL & CLS


::Comprueba que existan las herramientas
:CheckTools
cls
if not exist "Tools\fileslist.txt" (
	echo  ==============================================================================
	echo  ============== [fileslist.txt] is missing in "Tools" folder. =================
	echo  ==============================================================================
	echo  =================== Please, re-Install Epic_maker tool. ======================
	pause> NUL
	exit
)
set verificando=null
%CECHO% {aqua on black} // Checking tools // {#} & ECHO. & ECHO // Checking tools... // >> log.txt
	echo.
	echo.
%CECHO%  {0E}Found errors:{#}
	echo.
	echo.
for /f "delims=" %%a in ('type Tools\fileslist.txt') do if not exist "%%a" (
	set verificando=y
%CECHO%  {0C}[%%~na%%~xa]{#} is missing from Tools folder. & ECHO. & ECHO [%%~na%%~xa] is missing from Tools folder. >> log.txt
	echo.
	)
if "!verificando!"=="y" (
	echo  Please, re-Install Epic_maker tool.
	pause> NUL
	exit
)

ECHO - file tools are [OK] >> log.txt
ECHO. >> log.txt
IF NOT EXIST *.zip CLS & %CECHO%  {\n}{0C} ERROR FOUND:{#}{\n} & ECHO. & ECHO  THERE IS NOT .ZIP ROM FILE TO PROCESS & ECHO. & ECHO  BE SMART AND PASTE ONE .ZIP ROM INTO CURRENT FOLDER & ECHO // CANT CONTINUE, NO ZIP ROM FILE FOUND TO PROCESS // >> log.txt & PAUSE> NUL & exit

::Detecta SO y arquitectura
REG QUERY "HKLM\Hardware\Description\System\CentralProcessor\0" | FIND /i "x86" > NUL && SET OS=x32 || SET OS=x64
FOR /F "tokens=4-5 delims=. " %%i IN ('ver') DO SET VERSION=%%i.%%j
if "%version%" == "0.0" SET WINDOWS=Unknown
if "%version%" == "5.1" SET WINDOWS=Windows XP
if "%version%" == "6.0" SET WINDOWS=Windows Vista
if "%version%" == "6.1" SET WINDOWS=Windows 7
if "%version%" == "6.2" SET WINDOWS=Windows 8
if "%version%" == "6.3" SET WINDOWS=Windows 8.1
if "%version%" == "10.0" SET WINDOWS=Windows 10


::Edita el LOG
:EditLOG
ECHO. >> log.txt
CLS
%CECHO% {aqua on black} // System Properties // {#} & ECHO. & ECHO // System Properties // >> log.txt
ECHO.
ECHO *********************************************************
ECHO - Time: %TIME:~0,5% & ECHO - Time:%TIME:~0,5% >> log.txt
ECHO - Date: %DATE% & ECHO - Date: %DATE% >> log.txt
%CECHO% - ROM: {0F}%ZIP%{#} & ECHO - ROM: %ZIP% >> log.txt
ECHO.
ECHO - Operating System: %WINDOWS% - %OS% & ECHO - Operating System: %WINDOWS% - %OS% >> log.txt
ECHO *********************************************************
TIMEOUT /T 3 /nobreak > NUL & CLS & ECHO. >> log.txt


::Comprueba si hay una ROM
:CheckROM
%CECHO% {aqua on black} // Checking if exist a zip ROM // {#} & ECHO. & ECHO // Checking if exist a zip ROM // >> log.txt
ECHO.
ECHO ****************************************************************
IF EXIST *.zip %CECHO% - ROM %ZIP% {0A}was found{#} & ECHO - ROM %ZIP% was found >> log.txt
ECHO.
IF NOT EXIST *.zip ECHO - There is not a zip ROM on root folder. Please, copy one zip ROM & ECHO - There is not a zip ROM on root folder. Please, copy one zip ROM >> log.txt & PAUSE > NUL & EXIT
ECHO ****************************************************************
TIMEOUT /T 2 /nobreak > NUL & CLS & ECHO. >> log.txt

::Crea un proyecto
:MakeProject
MD 1-Project
CLS
%CECHO% {aqua on black} // Checking if exist another project // {#} & ECHO. & ECHO // Checking if exist another project // >> log.txt
ECHO.
IF EXIST 1-Project\%PROYECTO% ECHO - %PROYECTO% project was found & ECHO - %PROYECTO% project was found >> log.txt
cls
%CECHO% {aqua on black} // Not prior project found // {#} & ECHO. & ECHO - Not prior project found >> log.txt
ECHO.
SET alias=
CLS
echo :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
echo "           _                                                                ";
echo "          | |_  _   _  _ __    ___   _   _   ___   _   _  _ __              ";
echo "          | __|| | | || '_ \  / _ \ | | | | / _ \ | | | || '__|             ";
echo "          | |_ | |_| || |_) ||  __/ | |_| || (_) || |_| || |                ";
echo "           \__| \__, || .__/  \___|  \__, | \___/  \__,_||_|                ";
echo "                |___/ |_|  _         |___/                                  ";
echo "           _ __  (_)  ___ | | __ _ __    __ _  _ __ ___    ___              ";
echo "          | '_ \ | | / __|| |/ /| '_ \  / _\`|| '_ \` _ \ / _ \             ";
echo "          | | | || || (__ |   < | | | || (_| || | | | | ||  __/             ";
echo "          |_| |_||_| \___||_|\_\|_| |_| \__,_||_| |_| |_||\___|             ";
echo :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
echo.
%CECHO%                  {0B}For proper credits during installation{#}                                                                                                                     
echo.                                                                                                                      
ECHO.
ECHO.
SET /p alias= Type your alias: 
ECHO - User nickname is: !alias! >> log.txt
CLS
ECHO.
ECHO ******************************************************************
IF NOT EXIST %PROYECTO% MKDIR 1-Project\%PROYECTO% & %CECHO% - %PROYECTO% project {0A}was created{#} & ECHO - %PROYECTO% project was created >> log.txt
ECHO.
ECHO ******************************************************************
TIMEOUT /T 3 /nobreak > NUL & CLS & ECHO. >> log.txt

::Comprueba carpetas y las crea si no existen
:CheckFolders
RMDIR /Q /S Tools\3-Deodexer\logs
CLS
%CECHO% {aqua on black} // Making workable folders // {#} & ECHO. & ECHO // Making workable folders // >> log.txt
ECHO.
ECHO *****************************************************************************
IF NOT EXIST 1-Project\%PROYECTO%\1-Sources MKDIR 1-Project\%PROYECTO%\1-Sources | ECHO 1-Project\%PROYECTO%\1-Sources & ECHO - Creating folder 1-Project\%PROYECTO%\1-Sources >> log.txt
IF NOT EXIST 1-Project\%PROYECTO%\2-Converted-IMG MKDIR 1-Project\%PROYECTO%\2-Converted-IMG | ECHO 1-Project\%PROYECTO%\2-Converted-IMG & ECHO - Creating folder 1-Project\%PROYECTO%\2-Converted-IMG >> log.txt
IF NOT EXIST 1-Project\%PROYECTO%\3-IMG-Folder MKDIR 1-Project\%PROYECTO%\3-IMG-Folder | ECHO 1-Project\%PROYECTO%\3-IMG-Folder & ECHO - Creating folder 1-Project\%PROYECTO%\3-IMG-Folder >> log.txt
ECHO *****************************************************************************
TIMEOUT /T 2 /nobreak > NUL & CLS & ECHO. >> log.txt

::Identifica qué tipo de ROM trae el zip MIUI
:Looking_format
%CECHO% {aqua on black} // Identifying ROM zip type for //{\n}{\n}{0F}[%ZIP%]{#}{#} & ECHO. & ECHO // Identifying ROM zip type for "%ZIP%" // >> log.txt
IF NOT EXIST 1-Project\%PROYECTO%\ROMType MKDIR 1-Project\%PROYECTO%\ROMType
ECHO.
ECHO ******************************************
ECHO.
ECHO  -Processing...
Tools\7za.exe x "%ZIP%" -o1-Project\%PROYECTO%\ROMType -y> NUL >> log-7za.txt
IF EXIST 1-Project\%PROYECTO%\ROMType\system.new.dat IF EXIST 1-Project\%PROYECTO%\ROMType\system.transfer.list GOTO TipoDat
IF EXIST 1-Project\%PROYECTO%\ROMType\system.img GOTO TipoImg
IF EXIST 1-Project\%PROYECTO%\ROMType\system IF EXIST 1-Project\%PROYECTO%\ROMType\system\build.prop GOTO TipoCarpeta
TIMEOUT /T 5 /nobreak > NUL & CLS & ECHO. >> log.txt

:TipoDat
	CLS
	ECHO ******************************************
	%CECHO% - The zip ROM type is {0A}[system.new.DAT]{#} & ECHO. & ECHO - system.new.dat zip ROM type found >> log.txt & echo new.dat format >> dat_format.txt
	ECHO.
	ECHO ******************************************
	ECHO - Moving necessary files > NUL >> log.txt
	MOVE /y 1-Project\%PROYECTO%\ROMType\system.new.dat 1-Project\%PROYECTO%\1-Sources > NUL >> log.txt
	MOVE /y 1-Project\%PROYECTO%\ROMType\system.transfer.list 1-Project\%PROYECTO%\1-Sources > NUL >> log.txt
	MOVE /y 1-Project\%PROYECTO%\ROMType\file_contexts 1-Project\%PROYECTO%\1-Sources > NUL >> log.txt
	MOVE /y 1-Project\%PROYECTO%\ROMType\boot.img Tools\2-Boot > NUL >> log.txt
	MOVE /y 1-Project\%PROYECTO%\ROMType\META-INF 1-Project\%PROYECTO%\1-Sources > NUL >> log.txt
	RMDIR /Q /S 1-Project\%PROYECTO%\ROMType & ECHO -Removing ROMType folder >> log.txt
	TIMEOUT /T 5 /nobreak > NUL
	GOTO DATtoIMG

:TipoCarpeta
	CLS
	ECHO ******************************************
	%CECHO% - The zip ROM type is {0A}[system as folder]{#} & ECHO. & ECHO - system as folder zip ROM type found >> log.txt & echo system folder format >> folder_format.txt
	ECHO.
	ECHO ******************************************
	ECHO -Moving necessary files > NUL >> log.txt
	MOVE /y 1-Project\%PROYECTO%\ROMType\system 1-Project\%PROYECTO%\3-IMG-Folder > NUL >> log.txt
	MOVE /y 1-Project\%PROYECTO%\ROMType\file_contexts 1-Project\%PROYECTO%\1-Sources > NUL >> log.txt
	MOVE /y 1-Project\%PROYECTO%\ROMType\boot.img Tools\2-Boot > NUL >> log.txt
	MOVE /y 1-Project\%PROYECTO%\ROMType\META-INF 1-Project\%PROYECTO%\1-Sources > NUL >> log.txt
	RMDIR /Q /S 1-Project\%PROYECTO%\ROMType & ECHO -Removing ROMType folder >> log.txt
	FOR /F "Tokens=2* Delims==" %%# In (
    'TYPE "1-Project\%PROYECTO%\3-IMG-Folder\system\build.prop" ^| FINDSTR "ro.product.device="'
	) Do (
    SET "device=%%#"
	)
	if not "!device!"=="kenzo" if not "!device!"=="capricorn" if not "!device!"=="natrium" if not "!device!"=="lithium" if not "!device!"=="kate" if not "!device!"=="land" if not "!device!"=="nikel" if not "!device!"=="hennessy" if not "!device!"=="markw" if not "!device!"=="cancro" if not "!device!"=="ido" (
		cls
		echo. >>log.txt
		%CECHO% {aqua on black} // Checking e-maker device support // {#} & Echo. & Echo // Checking e-maker device support // >>log.txt
		echo.
		echo ***********************************************************
		%CECHO% - Sorry, but your {0c}%device%{#} device is not supported & echo. & ECHO. - %device% device is not supported >>log.txt
		echo ***********************************************************
		pause>nul
		goto exit
	)
	CLS
	ECHO ************************************************************
	%CECHO% - REMEMBER TO {0a}DELETE SYSTEM FOLDER{#} FROM ZIP ROM USING WINRAR
	ECHO.
	ECHO - DOING THIS, YOU ENSURE THAT SYSTEM UPDATE WILL BE RIGHT
	ECHO ************************************************************
	TIMEOUT /T 7 /nobreak > NUL
	goto DesSecureBoot

:TipoImg
	CLS
	ECHO ******************************************
	%CECHO% - The zip ROM type is {0A}[system.IMG]{#} & ECHO. & ECHO - system as folder zip ROM type found >> log.txt & echo .img format >> img_format.txt
	ECHO.
	ECHO ******************************************
	ECHO -Moving necessary files > NUL >> log.txt
	MOVE /y 1-Project\%PROYECTO%\ROMType\system.img 1-Project\%PROYECTO%\2-Converted-IMG > NUL >> log.txt
	MOVE /y 1-Project\%PROYECTO%\ROMType\file_contexts 1-Project\%PROYECTO%\1-Sources > NUL >> log.txt
	MOVE /y 1-Project\%PROYECTO%\ROMType\boot.img Tools\2-Boot > NUL >> log.txt
	MOVE /y 1-Project\%PROYECTO%\ROMType\META-INF 1-Project\%PROYECTO%\1-Sources > NUL >> log.txt
	RMDIR /Q /S 1-Project\%PROYECTO%\ROMType & ECHO -Removing ROMType folder >> log.txt
	TIMEOUT /T 5 /nobreak > NUL
	goto IMGtoSystem

::Convierte DAT a IMG
:DATtoIMG
ECHO. >> log.txt
CLS
%CECHO% {aqua on black} // Converting .DAT to .IMG // {#} & ECHO. & ECHO // Converting .DAT to .IMG // >> log.txt
ECHO.
ECHO ******************************************
ECHO - Converting DAT to IMG & ECHO - Converting DAT to IMG >> log.txt
Tools\sdat2img 1-Project\%PROYECTO%\1-Sources\system.transfer.list 1-Project\%PROYECTO%\1-Sources\system.new.dat 1-Project\%PROYECTO%\2-Converted-IMG\system.img >> log-SDAtoIMG.txt 
IF EXIST 1-Project\%PROYECTO%\2-Converted-IMG\system.img %CECHO% - system.img {0A}[OK]{#} & ECHO - system.img [OK] >> log.txt
ECHO.
IF NOT EXIST 1-Project\%PROYECTO%\2-Converted-IMG\system.img ECHO - Error when converting DAT to IMG & ECHO - Error when converting DAT to IMG >> log.txt & PAUSE> NUL & EXIT
ECHO ******************************************
DEL 1-Project\%PROYECTO%\1-Sources\system.transfer.list > NUL >> log.txt
DEL 1-Project\%PROYECTO%\1-Sources\system.new.dat > NUL >> log.txt
TIMEOUT /T 5 /nobreak > NUL & CLS & ECHO. >> log.txt

::Convierte IMG a /system
:IMGtoSystem
ECHO. >> log.txt
SET updater_script=1-Project\%PROYECTO%\1-Sources\META-INF\com\google\android\updater-script
XCOPY 1-Project\%PROYECTO%\1-Sources\META-INF\com\google\android\updater-script "%~dp0" /y >nul
RENAME updater-script updater-script2 >nul
MOVE /y updater-script2 1-Project\%PROYECTO%\1-Sources\META-INF\com\google\android >nul
SET updater_script2=1-Project\%PROYECTO%\1-Sources\META-INF\com\google\android\updater-script2
CLS
%CECHO% {aqua on black} // Converting .IMG to system // {#} & ECHO. & ECHO // Converting .IMG to system // >> log.txt
ECHO.
FOR /R 1-Project\%PROYECTO%\2-Converted-IMG %%A IN (*) DO echo system.img.size=%%~zA>>img_size.txt
FOR /F "Tokens=2* Delims==" %%# In (
    'TYPE "img_size.txt" ^| FINDSTR "system.img.size="'
) Do (
    SET "img_size=%%#"
)
ECHO - Default system.img size is %img_size% >> log.txt
ECHO ***********************************
ECHO - Extracting system.img & ECHO - Extracting system.img >> log.txt
Tools\7za x -y "1-Project\%PROYECTO%\2-Converted-IMG\system.img" -o"1-Project\system" >nul
!busybox! rm -rf 1-Project/system/[SYS]
!busybox! rm -rf 1-Project/%PROYECTO%/2-Converted-IMG/system.img
call :write_symlinks
start Tools\patches\patchsyms.bat >nul
tools\dos2unix Tools/patches/updater-maker/original_symlinks >nul
move /y 1-Project\system 1-Project\%PROYECTO%\3-IMG-Folder >nul
IF EXIST 1-Project\%PROYECTO%\3-IMG-Folder\system %CECHO% - system extracted {0A}[OK]{#} & ECHO - system extracted [OK] >> log.txt
echo.
IF NOT EXIST 1-Project\%PROYECTO%\3-IMG-Folder\system ECHO - Error found extracting system.img & ECHO - Error found extracting system.img >> log.txt & PAUSE> NUL & EXIT
CLS
if exist Tools\patches\updater-maker\original_symlinks %CECHO% - symlinks have been saved {0A}[OK]{#} & ECHO - symlinks have been saved [OK] >> log.txt
echo.
ECHO ***********************************
TIMEOUT /T 3 /nobreak > NUL & CLS & ECHO. >> log.txt

:Check_support
IF EXIST 1-Project\%PROYECTO%\3-IMG-Folder\system\build.prop %CECHO% - Build.prop {0A}[OK]{#} & ECHO - Build.prop [OK] >> log.txt
IF NOT EXIST 1-Project\%PROYECTO%\3-IMG-Folder\system\build.prop ECHO - No build.prop found & ECHO - No build.prop found >> log.txt & PAUSE> NUL & EXIT
	FOR /F "Tokens=2* Delims==" %%# In (
    'TYPE "1-Project\%PROYECTO%\3-IMG-Folder\system\build.prop" ^| FINDSTR "ro.product.device="'
	) Do (
    SET "device=%%#"
	)
	if not "!device!"=="kenzo" if not "!device!"=="capricorn" if not "!device!"=="lithium" if not "!device!"=="natrium" if not "!device!"=="kate" if not "!device!"=="land" if not "!device!"=="nikel" if not "!device!"=="hennessy" if not "!device!"=="markw" if not "!device!"=="cancro" if not "!device!"=="ido" (
		cls
		echo. >>log.txt
		%CECHO% {aqua on black} // Checking e-maker device support // {#} & Echo. & Echo // Checking e-maker device support // >>log.txt
		echo.
		echo ***********************************************************
		%CECHO% - Sorry, but your {0c}%device%{#} device is not supported & echo. & ECHO. - %device% device is not supported >>log.txt
		echo ***********************************************************
		pause>nul
		goto exit
	)

	
::Desactivar modo seguro en el Kernel
:DesSecureBoot
CLS
%CECHO% {aqua on black} // Disabling  kernel encryption // {#} & ECHO. & ECHO // Disabling  kernel encryption // >> log.txt
ECHO.
CD Tools\2-Boot
start unpackimg.bat boot.img
CD "%~dp0"
ECHO - Disabling  kernel data and boot encryption & ECHO - Disabling  kernel data and boot encryption >> log.txt
ECHO  **********************************************************
ECHO.
%CECHO%  {0E}This step is a manual process{#}:
ECHO.
ECHO.
ECHO  **********************************************************
ECHO.
ECHO  1- Please, go to "Tools\2-Boot" folder.
%CECHO%  2- Hit {0B}1-easy-patch.bat{#} for patch security issue.
ECHO.
ECHO.
ECHO  **********************************************************
ECHO.
ECHO.
ECHO  When compress process is finished, close pop up window
%CECHO%  and hit {0A}ENTER{#} key on this screen to patch kernel.
ECHO.
PAUSE> NUL
MOVE /y Tools\2-Boot\boot.img "%~dp0"
CLS
CLS
ECHO  **********************************************************
%CECHO%  {0A}New patched boot.img was generated{#}
ECHO.
ECHO  **********************************************************
TIMEOUT /T 2 /nobreak > NUL & CLS & ECHO. >> log.txt

	
	
:deodex_status
::Revisa estado de deodexado de la ROM
CLS
echo. >> log.txt
%CECHO% {aqua on black} // Checking deodex status // {#} & ECHO. & ECHO // Checking deodex status // >> log.txt
ECHO.
ECHO.  -For make any system customization, this ROM must be deodexed
ECHO.
ECHO **************************
%CECHO%  {0F}-Checking status...{#}
ECHO.
TIMEOUT /T 3 /nobreak > NUL & ECHO.
IF EXIST 1-Project\%PROYECTO%\3-IMG-Folder\system\framework\oat\arm\*.odex IF EXIST 1-Project\%PROYECTO%\3-IMG-Folder\system\framework\arm\*.odex IF EXIST 1-Project\%PROYECTO%\3-IMG-Folder\system\framework\arm64\*.odex %CECHO%  {0C}-System is Not Deodexed{#} & ECHO ************************** & ECHO - ROM system is not deodexed >> log.txt & GOTO Deodexar
IF NOT EXIST 1-Project\%PROYECTO%\3-IMG-Folder\system\framework\oat\arm\*.odex IF NOT EXIST 1-Project\%PROYECTO%\3-IMG-Folder\system\framework\arm\*.odex IF NOT EXIST 1-Project\%PROYECTO%\3-IMG-Folder\system\framework\arm64\*.odex %CECHO%  {0A}-System is Deodexed{#} & ECHO. & ECHO ************************** & ECHO - ROM system is deodexed >> log.txt & TIMEOUT /T 4 /nobreak > NUL & GOTO GetDevice
::IF EXIST 1-Project\%PROYECTO%\3-IMG-Folder\system\framework\oat\arm\*.odex IF EXIST 1-Project\%PROYECTO%\3-IMG-Folder\system\framework\oat\arm64\*.odex %CECHO%  {0C}-System is Not Deodexed{#} & ECHO. & ECHO ************************** & ECHO -ROM system is not deodexed // >> log.txt & TIMEOUT /T 4 /nobreak > NUL & GOTO Deodexar
::IF NOT EXIST 1-Project\%PROYECTO%\3-IMG-Folder\system\framework\oat\arm\*.odex IF NOT EXIST 1-Project\%PROYECTO%\3-IMG-Folder\system\framework\oat\arm64\*.odex %CECHO%  {0A}-System is Deodexed{#} & ECHO. & ECHO ************************** & ECHO -ROM system is deodexed // >> log.txt & TIMEOUT /T 4 /nobreak > NUL & GOTO GetDevice


:Deodexar
::Proceso manual de deodexado con lordroid deodexer
CLS
%CECHO% {aqua on black} // Checking deodex status // {#}
ECHO.
ECHO  -For make any system customization, this ROM must be deodexed
ECHO.
ECHO **************************
%CECHO%  {0F}-Checking status...{#}
ECHO.
ECHO **************************
%CECHO%  {0C}-Not deodexed ROM{#} & ECHO - ROM not deodexed >> log.txt
ECHO.
ECHO **************************
TIMEOUT /T 3 /nobreak > NUL & ECHO. >> log.txt
CLS
ECHO.
START Tools\3-Deodexer\Launcher.jar
ECHO.
ECHO ****************************************************************
%CECHO%  {0F}Please, start deodex process manually...{#}
ECHO.
ECHO.
%CECHO%  {0B}INSTRUCTIONS:{#}
ECHO.
ECHO.
%CECHO%  -GO TO {0F}Tools\3-Deodexer{#}, OPEN {0F}Launcher.jar{#}
ECHO.
%CECHO%  -SEARCH {0F}SYSTEM{#} FOLDER LOCATED ON
ECHO.
%CECHO%   1-Project\YOU-ROM-PROJECT\3-IMG-Folder AND {0F}DROP IT{#} INTO
ECHO.
%CECHO%   BROWSER BAR FROM LORDROID DEODEXER APP.
ECHO.
%CECHO%  -WHEN FOLDER IS CORRECTLY SET, THEN HIT {0A}DEODEX NOW!{#} BUTTON
ECHO.
ECHO ****************************************************************
ECHO.
ECHO.
%CECHO%  -When you finish the deodex process, hit {0F}ENTER{#} to continue.
ECHO.
ECHO.
PAUSE> NUL
RMDIR /Q /S 1-Project\%PROYECTO%\3-IMG-Folder\system\framework\arm
RMDIR /Q /S 1-Project\%PROYECTO%\3-IMG-Folder\system\framework\arm64

::Comprueba informacion en build.prop
:GetDevice
CLS
ECHO. >> log.txt
%CECHO% {aqua on black} // Getting build.prop info // {#} & ECHO. & ECHO // Getting build.prop info // >> log.txt
ECHO.
ECHO *************************************
IF EXIST 1-Project\%PROYECTO%\3-IMG-Folder\system\build.prop %CECHO% - Build.prop {0A}[OK]{#} & ECHO - Build.prop [OK] >> log.txt
IF NOT EXIST 1-Project\%PROYECTO%\3-IMG-Folder\system\build.prop ECHO - No build.prop found & ECHO - No build.prop found >> log.txt & PAUSE> NUL & EXIT
ECHO.
ECHO *************************************
ECHO.
ECHO.
ECHO *************************************
%CECHO% - HI {0A}%alias%{#}
ECHO.
ECHO *************************************
%CECHO% - Your xiaomi model is: {0A}%device%{#} & ECHO - Your xiaomi model is: %device% >> log.txt
ECHO.
ECHO -------------------------------------
IF NOT DEFINED device ECHO - No model info & ECHO - No model info >> log.txt & PAUSE> NUL & EXIT
FOR /F "Tokens=2* Delims==" %%# In (
    'TYPE "1-Project\%PROYECTO%\3-IMG-Folder\system\build.prop" ^| FINDSTR "ro.build.version.release="'
) Do (
    SET "release=%%#"
)
%CECHO% - Your android version is: {0A}%release%{#} & ECHO - Your android version is: %release% >> log.txt
ECHO.
ECHO -------------------------------------
IF NOT DEFINED release ECHO - Error getting info & ECHO - Error getting info >> log.txt & PAUSE> NUL & EXIT
FOR /F "Tokens=2* Delims==" %%# In (
    'TYPE "1-Project\%PROYECTO%\3-IMG-Folder\system\build.prop" ^| FINDSTR "ro.build.version.incremental="'
) Do (
    SET "miui=%%#"
)
%CECHO% - The MIUI version is: {0A}%miui%{#} & ECHO - The MIUI version is %miui% >> log.txt
ECHO.
IF NOT DEFINED miui ECHO - Error getting info from MIUI & ECHO - Error getting info from MIUI >> log.txt & PAUSE> NUL & EXIT
ECHO *************************************
TIMEOUT /T 3 /nobreak > NUL & CLS & ECHO. >> log.txt

::Agregar globalización
:Adding_global
ECHO  ***********************************************
%CECHO%  {0A}Removing official OTA{#} & ECHO. & ECHO // Removing official OTA // >> log.txt
ECHO.
ECHO  ***********************************************
!busybox! sed -i s/"ro.product.mod_device=!device!_global/ro.product.mod_device=!device!_epic_global/" 1-Project\%PROYECTO%\3-IMG-Folder\system\build.prop
!busybox! sed -i s/"ro.product.mod_device=!device!_global/ro.product.mod_device=!device!_epic_global/" 1-Project\%PROYECTO%\3-IMG-Folder\system\build.prop
ECHO ro.product.mod_device=!device!_epic_global >> 1-Project\%PROYECTO%\3-IMG-Folder\system\build.prop
ECHO - Added !device!_epic_global to build.prop >> log.txt
TIMEOUT /T 3 /nobreak > NUL & CLS & ECHO. >> log.txt



:LineasEpicas
::Editar build.prop
%CECHO% {aqua on black} // Adding tweak lines to build.prop // {#} & ECHO. & ECHO // Adding tweak lines to build.prop // >> log.txt
ECHO.
ECHO.
TYPE Tools\patches\epic_things\buildadds.txt >> 1-Project\%PROYECTO%\3-IMG-Folder\system\build.prop
ECHO ***************************************************************************
%CECHO%  {0A}-Added lines were:{#} Tools\patches\epic_things\buildadds.txt & ECHO. & ECHO - Added Tools\patches\epic_things\buildadds.txt lines >> log.txt
ECHO ***************************************************************************
TIMEOUT /T 3 /nobreak > NUL & CLS & ECHO. >> log.txt


:debloater
::Eliminación de aplicaciones no necesarias
%CECHO% {aqua on black} // Removing MIUI bloatware // {#} & ECHO. & ECHO // Removing MIUI bloatware // >> log.txt
ECHO.
ECHO ***************************************************************
ECHO  -This script was prepared only for working on Android 5+ ROMs
ECHO ***************************************************************
ECHO.
TIMEOUT /T 5 /nobreak > NUL
del 1-Project\%PROYECTO%\3-IMG-Folder\system\recovery-from-boot.p> NUL
rmdir /Q /S 1-Project\%PROYECTO%\3-IMG-Folder\system\data-app\Facebook> NUL
rmdir /Q /S 1-Project\%PROYECTO%\3-IMG-Folder\system\data-app\klobugreport> NUL
rmdir /Q /S 1-Project\%PROYECTO%\3-IMG-Folder\system\app\AnalyticsCore> NUL
rmdir /Q /S 1-Project\%PROYECTO%\3-IMG-Folder\system\app\Chrome> NUL
rmdir /Q /S 1-Project\%PROYECTO%\3-IMG-Folder\system\app\BasicDreams> NUL
rmdir /Q /S 1-Project\%PROYECTO%\3-IMG-Folder\system\app\Drive> NUL
rmdir /Q /S 1-Project\%PROYECTO%\3-IMG-Folder\system\app\Duo> NUL
rmdir /Q /S 1-Project\%PROYECTO%\3-IMG-Folder\system\app\Gmail2> NUL
rmdir /Q /S 1-Project\%PROYECTO%\3-IMG-Folder\system\app\GoogleCalendarSyncAdapter> NUL
rmdir /Q /S 1-Project\%PROYECTO%\3-IMG-Folder\system\app\GoogleContactsSyncAdapter> NUL
rmdir /Q /S 1-Project\%PROYECTO%\3-IMG-Folder\system\app\GoogleTTS> NUL
rmdir /Q /S 1-Project\%PROYECTO%\3-IMG-Folder\system\app\Hangouts> NUL
rmdir /Q /S 1-Project\%PROYECTO%\3-IMG-Folder\system\app\LatinImeGoogle> NUL
rmdir /Q /S 1-Project\%PROYECTO%\3-IMG-Folder\system\app\mab> NUL
rmdir /Q /S 1-Project\%PROYECTO%\3-IMG-Folder\system\app\Maps> NUL
rmdir /Q /S 1-Project\%PROYECTO%\3-IMG-Folder\system\app\MiDiscover> NUL
rmdir /Q /S 1-Project\%PROYECTO%\3-IMG-Folder\system\app\MiPicks> NUL
rmdir /Q /S 1-Project\%PROYECTO%\3-IMG-Folder\system\app\Music2> NUL
rmdir /Q /S 1-Project\%PROYECTO%\3-IMG-Folder\system\app\PaymentService> NUL
rmdir /Q /S 1-Project\%PROYECTO%\3-IMG-Folder\system\app\PhaseBeam> NUL
rmdir /Q /S 1-Project\%PROYECTO%\3-IMG-Folder\system\app\Photos> NUL
rmdir /Q /S 1-Project\%PROYECTO%\3-IMG-Folder\system\app\PhotoTable> NUL
rmdir /Q /S 1-Project\%PROYECTO%\3-IMG-Folder\system\app\YouTube> NUL
rmdir /Q /S 1-Project\%PROYECTO%\3-IMG-Folder\system\app\Galaxy4> NUL
rmdir /Q /S 1-Project\%PROYECTO%\3-IMG-Folder\system\app\GameCenter> NUL
rmdir /Q /S 1-Project\%PROYECTO%\3-IMG-Folder\system\app\HoloSpiralWallpaper> NUL
rmdir /Q /S 1-Project\%PROYECTO%\3-IMG-Folder\system\app\LiveWallpapers> NUL
rmdir /Q /S 1-Project\%PROYECTO%\3-IMG-Folder\system\app\MiLivetalk> NUL
rmdir /Q /S 1-Project\%PROYECTO%\3-IMG-Folder\system\app\Mipay> NUL
rmdir /Q /S 1-Project\%PROYECTO%\3-IMG-Folder\system\app\MiuiSuperMarket > NUL
rmdir /Q /S 1-Project\%PROYECTO%\3-IMG-Folder\system\app\NoiseField> NUL
rmdir /Q /S 1-Project\%PROYECTO%\3-IMG-Folder\system\app\SogouInput\oat> NUL
del 1-Project\%PROYECTO%\3-IMG-Folder\system\app\SogouInput\SogouInput.apk> NUL
rmdir /Q /S 1-Project\%PROYECTO%\3-IMG-Folder\system\app\SystemAdSolution> NUL
rmdir /Q /S 1-Project\%PROYECTO%\3-IMG-Folder\system\app\VoiceAssist> NUL
rmdir /Q /S 1-Project\%PROYECTO%\3-IMG-Folder\system\app\Videos> NUL
rmdir /Q /S 1-Project\%PROYECTO%\3-IMG-Folder\system\app\WebViewGoogle> NUL
rmdir /Q /S 1-Project\%PROYECTO%\3-IMG-Folder\system\app\XiaomiVip> NUL
rmdir /Q /S 1-Project\%PROYECTO%\3-IMG-Folder\system\app\XMPass\oat> NUL
del 1-Project\%PROYECTO%\3-IMG-Folder\system\app\XMPass\XMPass.apk> NUL
rmdir /Q /S 1-Project\%PROYECTO%\3-IMG-Folder\system\data-app\AdEcommerce> NUL
rmdir /Q /S 1-Project\%PROYECTO%\3-IMG-Folder\system\data-app\klobugreport> NUL
rmdir /Q /S 1-Project\%PROYECTO%\3-IMG-Folder\system\data-app\MiFinance> NUL
rmdir /Q /S 1-Project\%PROYECTO%\3-IMG-Folder\system\data-app\MiuiBBS> NUL
rmdir /Q /S 1-Project\%PROYECTO%\3-IMG-Folder\system\data-app\O2O> NUL
rmdir /Q /S 1-Project\%PROYECTO%\3-IMG-Folder\system\data-app\VTalk> NUL
rmdir /Q /S 1-Project\%PROYECTO%\3-IMG-Folder\system\data-app\WaliLive> NUL
rmdir /Q /S 1-Project\%PROYECTO%\3-IMG-Folder\system\data-app\> NUL
rmdir /Q /S 1-Project\%PROYECTO%\3-IMG-Folder\system\priv-app\MiGameCenterSDKService> NUL
rmdir /Q /S 1-Project\%PROYECTO%\3-IMG-Folder\system\priv-app\GmsCore> NUL
rmdir /Q /S 1-Project\%PROYECTO%\3-IMG-Folder\system\priv-app\GoogleBackupTransport> NUL
rmdir /Q /S 1-Project\%PROYECTO%\3-IMG-Folder\system\priv-app\GoogleFeedback> NUL
rmdir /Q /S 1-Project\%PROYECTO%\3-IMG-Folder\system\priv-app\GoogleLoginService> NUL
rmdir /Q /S 1-Project\%PROYECTO%\3-IMG-Folder\system\priv-app\GoogleOneTimeInitializer> NUL
rmdir /Q /S 1-Project\%PROYECTO%\3-IMG-Folder\system\priv-app\GooglePartnerSetup> NUL
rmdir /Q /S 1-Project\%PROYECTO%\3-IMG-Folder\system\priv-app\MiuiVoip> NUL
rmdir /Q /S 1-Project\%PROYECTO%\3-IMG-Folder\system\priv-app\VirtualSim> NUL
rmdir /Q /S 1-Project\%PROYECTO%\3-IMG-Folder\system\priv-app\GoogleServicesFramework> NUL
rmdir /Q /S 1-Project\%PROYECTO%\3-IMG-Folder\system\priv-app\Phonesky> NUL
rmdir /Q /S 1-Project\%PROYECTO%\3-IMG-Folder\system\priv-app\SetupWizard> NUL
rmdir /Q /S 1-Project\%PROYECTO%\3-IMG-Folder\system\priv-app\Velvet> NUL
del 1-Project\%PROYECTO%\3-IMG-Folder\system\priv-app\YellowPage\YellowPage.apk> NUL
rmdir /Q /S 1-Project\%PROYECTO%\3-IMG-Folder\system\priv-app\YellowPage\oat> NUL
ECHO - Miui bloatware applications were eliminated >> log.txt
ECHO. >> log.txt

:Adding_apps
::Extrae aplicaciones MIUI necesarias
CLS
%CECHO% {aqua on black} // Adding some applications // {#} & ECHO. & ECHO // Adding some applications // >> log.txt
ECHO  ***********************************************
Tools\7za.exe x Tools\patches\epic_things\epic-patcher.7z -o1-Project\%PROYECTO%\3-IMG-Folder -y >> log-7za.txt
IF NOT EXIST 1-Project\%PROYECTO%\1-Sources\META-INF\epic MKDIR 1-Project\%PROYECTO%\1-Sources\META-INF\epic
IF NOT EXIST 1-Project\%PROYECTO%\1-Sources\META-INF\epic\gapps MKDIR 1-Project\%PROYECTO%\1-Sources\META-INF\epic\gapps
ECHO.
if "!device!"=="nikel" if "!device!"=="kenzo" if "!device!"=="capricorn" if "!device!"=="lithium" if "!device!"=="natrium" if "!device!"=="kate" if "!device!"=="cancro" do (
	ECHO - Exporting Google Apps 6.0 >> log.txt
	XCOPY Tools\patches\GApps-zip\arm64\6.0.1\gapps-arm64-pico.zip 1-Project\%PROYECTO%\1-Sources\META-INF\epic\gapps /y > NUL
	GOTO AddOptions
)
if "!device!"=="hennessy" if "!device!"=="hermes" do (
	ECHO - Exporting Google Apps 5.0 >> log.txt
	XCOPY Tools\patches\GApps-zip\arm64\5.0.2\gapps-arm64-pico.zip 1-Project\%PROYECTO%\1-Sources\META-INF\epic\gapps /y > NUL
	GOTO AddOptions
)
if "!device!"=="ido" do (
	ECHO - Exporting Google Apps 5.1 >> log.txt
	XCOPY Tools\patches\GApps-zip\arm64\5.1\gapps-arm64-pico.zip 1-Project\%PROYECTO%\1-Sources\META-INF\epic\gapps /y > NUL
	GOTO AddOptions
)


::Añade opciones en la MIUISystem.apk
:AddOptions
IF EXIST 1-Project\%PROYECTO%\1-Sources\META-INF\epic\gapps\gapps-arm64-pico.zip %CECHO% {0A}- GApps [OK]{#} & ECHO. & ECHO - GApps have been added [OK] >> log.txt
ECHO. >> log.txt
CLS
%CECHO% {aqua on black} // Adding features to miuisystem.APK // {#} & ECHO. & ECHO // Adding features to miuisystem.APK // >> log.txt
ECHO.
ECHO *************************************
Tools\7za.exe e 1-Project\%PROYECTO%\3-IMG-Folder\system\app\miuisystem\miuisystem.apk assets\device_features\%device%.xml -aoa >> log-7za.txt| ECHO - Extracting %device%.xml & ECHO - Extracting %device%.xml >> log.txt
IF EXIST %device%.xml %CECHO% - %device%.xml {0A}[OK]{#} & ECHO - %device%.xml [OK] >> log.txt
ECHO.
IF NOT EXIST %device%.xml ECHO - Error found extracting %device%.xml & ECHO - Error found extracting %device%.xml >> log.txt & GOTO CPU_Manufacturer
!busybox! sed -i s/"name=\"support_camera_movie_solid\">false</name=\"support_camera_movie_solid\">true</" %device%.xml | ECHO - Adding object tracking & ECHO - Adding object tracking >> log.txt
!busybox! sed -i s/"name=\"support_camera_aohdr\">false</name=\"support_camera_aohdr\">true</" %device%.xml | ECHO - Enabling HDR & ECHO - Enabling HDR >> log.txt
!busybox! sed -i s/"name=\"support_camera_4k_quality\">false</name=\"support_camera_4k_quality\">true</" %device%.xml | ECHO - Adding 4K & ECHO - Adding 4K >> log.txt
!busybox! sed -i s/"name=\"support_touch_sensitive\">false</name=\"support_touch_sensitive\">true</" %device%.xml | ECHO - Adding glove mode & ECHO - Adding glove mode >> log.txt
RENAME 1-Project\%PROYECTO%\3-IMG-Folder\system\app\miuisystem\miuisystem.apk miuisystem.zip
MKDIR assets\device_features
MOVE %device%.xml assets\device_features > NUL
Tools\7za.exe a 1-Project\%PROYECTO%\3-IMG-Folder\system\app\miuisystem\miuisystem.zip assets\device_features\%device%.xml >> log-7za.txt
ECHO - Re-packing MIUISystem.apk & ECHO - Re-packing MIUISystem.apk >> log.txt 
RENAME 1-Project\%PROYECTO%\3-IMG-Folder\system\app\miuisystem\miuisystem.zip miuisystem.apk
IF EXIST 1-Project\%PROYECTO%\3-IMG-Folder\system\app\miuisystem\miuisystem.apk ECHO - MIUISystem.apk [OK] & ECHO - MIUISystem.apk [OK] >> log.txt
IF NOT EXIST 1-Project\%PROYECTO%\3-IMG-Folder\system\app\miuisystem\miuisystem.apk ECHO - Error when re-packing MIUISystem.apk & ECHO - Error when re-packing MIUISystem.apk >> log.txt & PAUSE> NUL & EXIT
RMDIR /Q /S assets
TIMEOUT /T 5 /nobreak > NUL & CLS & ECHO. >> log.txt

::Saber si el dispositivo lleva procesador qualcomm o MTK
:CPU_Manufacturer
CLS
IF EXIST 1-Project\%PROYECTO%\3-IMG-Folder\system\framework\mediatek-res\mediatek-res.apk GOTO Entorno_MTK > NUL >> log.txt 
GOTO Entorno_SD > NUL
TIMEOUT /T 2 /nobreak > NUL & CLS & ECHO. >> log.txt


::Preparar entorno MTK
:Entorno_MTK
CLS
ECHO - This %device% device, has a MTK Processor >> log.txt
ECHO. >> log.txt
%CECHO% {aqua on black} // Preparing epic environment //{#} & ECHO. & ECHO // Preparing epic environment // >> log.txt
ECHO.
ECHO ***************************************
ECHO Preparing environment for Epic Mods...
XCOPY 1-Project\%PROYECTO%\3-IMG-Folder\system\app\miuisystem\miuisystem.apk Tools\1-JBart /y> NUL
XCOPY 1-Project\%PROYECTO%\3-IMG-Folder\system\priv-app\Settings\Settings.apk Tools\1-JBart /y> NUL
XCOPY 1-Project\%PROYECTO%\3-IMG-Folder\system\framework\services.jar Tools\1-JBart /y> NUL
XCOPY 1-Project\%PROYECTO%\3-IMG-Folder\system\app\miuisystem\miuisystem.apk Tools\1-JBart\data\frameworks_single /y> NUL
XCOPY 1-Project\%PROYECTO%\3-IMG-Folder\system\app\miui\miui.apk Tools\1-JBart\data\frameworks_single /y> NUL
XCOPY 1-Project\%PROYECTO%\3-IMG-Folder\system\framework\framework-ext-res\framework-ext-res.apk Tools\1-JBart\data\frameworks_single /y> NUL
XCOPY 1-Project\%PROYECTO%\3-IMG-Folder\system\framework\framework-res.apk Tools\1-JBart\data\frameworks_single /y> NUL
XCOPY 1-Project\%PROYECTO%\3-IMG-Folder\system\framework\mediatek-res\mediatek-res.apk Tools\1-JBart\data\frameworks_single /y> NUL
ECHO.
%CECHO% - Patching {0A}Settings.apk{#} & ECHO - Patching Settings.apk >> log.txt
ECHO.
java -jar Tools\1-JBart\data\Tools\lib\apktool_2.1.1_Fluxed.jar if Tools\1-JBart\data\frameworks_single\miui.apk> NUL
java -jar Tools\1-JBart\data\Tools\lib\apktool_2.1.1_Fluxed.jar if Tools\1-JBart\data\frameworks_single\miuisystem.apk> NUL
java -jar Tools\1-JBart\data\Tools\lib\apktool_2.1.1_Fluxed.jar if Tools\1-JBart\data\frameworks_single\framework-res.apk> NUL
java -jar Tools\1-JBart\data\Tools\lib\apktool_2.1.1_Fluxed.jar if Tools\1-JBart\data\frameworks_single\framework-ext-res.apk> NUL
java -jar Tools\1-JBart\data\Tools\lib\apktool_2.1.1_Fluxed.jar if Tools\1-JBart\data\frameworks_single\mediatek-res.apk> NUL
java -jar Tools\1-JBart\data\Tools\lib\apktool_2.1.1_Fluxed.jar d -f Tools\1-JBart\Settings.apk -o Tools\1-JBart\Settings\decompiled> NUL
CALL Tools\patches\patchsett.bat
CALL Tools\patches\patchsett-old.bat
ROBOCOPY Tools\patches\Settings\res Tools\1-JBart\Settings\decompiled\res /e> NUL
ECHO // Recompiling Settings.apk //>> log.txt
java -jar Tools\1-JBart\data\Tools\lib\apktool_2.1.1_Fluxed.jar if Tools\1-JBart\data\frameworks_single\miui.apk> NUL
java -jar Tools\1-JBart\data\Tools\lib\apktool_2.1.1_Fluxed.jar if Tools\1-JBart\data\frameworks_single\miuisystem.apk> NUL
java -jar Tools\1-JBart\data\Tools\lib\apktool_2.1.1_Fluxed.jar if Tools\1-JBart\data\frameworks_single\framework-res.apk> NUL
java -jar Tools\1-JBart\data\Tools\lib\apktool_2.1.1_Fluxed.jar if Tools\1-JBart\data\frameworks_single\framework-ext-res.apk> NUL
java -jar Tools\1-JBart\data\Tools\lib\apktool_2.1.1_Fluxed.jar if Tools\1-JBart\data\frameworks_single\mediatek-res.apk> NUL
java -jar Tools\1-JBart\data\Tools\lib\apktool_2.1.1_Fluxed.jar b Tools\1-JBart\Settings\decompiled -o Tools\1-JBart\Settings\compiled\Settings.apk -c> NUL >> log.txt
XCOPY Tools\1-JBart\Settings\compiled\Settings.apk 1-Project\%PROYECTO%\3-IMG-Folder\system\priv-app\Settings /e /i /h /y>nul
CLS
ECHO ***************************************
IF EXIST Tools\1-JBart\Settings\compiled\Settings.apk %CECHO% - Settings.apk {0A}patched{#} & ECHO. & ECHO - Settings.apk patched>> log.txt
ECHO.
IF NOT EXIST Tools\1-JBart\Settings\compiled\Settings.apk %CECHO% - Settings.apk {0C}NOT patched{#} & ECHO. & ECHO - Settings.apk NOT patched>> log.txt
ECHO.
RMDIR /Q /S Tools\1-JBart\Settings
DEL Tools\1-JBart\Settings.apk
TIMEOUT /T 3 /nobreak > NUL & CLS & ECHO. >> log.txt
GOTO Parches_JBart


::Preparar entorno Qualcomm
:Entorno_SD
CLS
ECHO. >> log.txt
%CECHO% {aqua on black} // Preparing epic environment //{#} & ECHO. & ECHO // Preparing epic environment // >> log.txt
ECHO.
ECHO ***************************************
ECHO - This %device% device, has a Qualcomm Processor >> log.txt
ECHO - Preparing environment for Epic Mods...
XCOPY 1-Project\%PROYECTO%\3-IMG-Folder\system\app\miuisystem\miuisystem.apk Tools\1-JBart /y> NUL
XCOPY 1-Project\%PROYECTO%\3-IMG-Folder\system\priv-app\Settings\Settings.apk Tools\1-JBart /y> NUL
XCOPY 1-Project\%PROYECTO%\3-IMG-Folder\system\framework\services.jar Tools\1-JBart /y> NUL
if "!device!"=="hennessy" if "!device!"=="hermes" if "!device!"=="ido" XCOPY 1-Project\%PROYECTO%\3-IMG-Folder\system\framework\android.policy.jar Tools\1-JBart /y> NUL
XCOPY 1-Project\%PROYECTO%\3-IMG-Folder\system\app\miuisystem\miuisystem.apk Tools\1-JBart\data\frameworks_single /y> NUL
IF EXIST Tools\1-JBart\data\frameworks_single\miuisystem.apk ECHO - miuisystem.apk set as framework >> log.txt
XCOPY 1-Project\%PROYECTO%\3-IMG-Folder\system\app\miui\miui.apk Tools\1-JBart\data\frameworks_single /y> NUL
IF EXIST Tools\1-JBart\data\frameworks_single\miui.apk ECHO - miui.apk set as framework >> log.txt
XCOPY 1-Project\%PROYECTO%\3-IMG-Folder\system\framework\framework-ext-res\framework-ext-res.apk Tools\1-JBart\data\frameworks_single /y> NUL
IF EXIST Tools\1-JBart\data\frameworks_single\framework-ext-res.apk ECHO - framework-ext-res.apk set as framework >> log.txt
XCOPY 1-Project\%PROYECTO%\3-IMG-Folder\system\framework\framework-res.apk Tools\1-JBart\data\frameworks_single /y> NUL
IF EXIST Tools\1-JBart\data\frameworks_single\framework-res.apk ECHO - framework-res.apk set as framework >> log.txt
ECHO.
ECHO ***************************************
%CECHO% - Patching {0A}Settings.apk{#} & ECHO - Patching Settings.apk >> log.txt
ECHO.
ECHO ***************************************
java -jar Tools\1-JBart\data\Tools\lib\apktool_2.1.1_Fluxed.jar if Tools\1-JBart\data\frameworks_single\miui.apk > NUL
java -jar Tools\1-JBart\data\Tools\lib\apktool_2.1.1_Fluxed.jar if Tools\1-JBart\data\frameworks_single\miuisystem.apk > NUL
java -jar Tools\1-JBart\data\Tools\lib\apktool_2.1.1_Fluxed.jar if Tools\1-JBart\data\frameworks_single\framework-res.apk > NUL
java -jar Tools\1-JBart\data\Tools\lib\apktool_2.1.1_Fluxed.jar if Tools\1-JBart\data\frameworks_single\framework-ext-res.apk > NUL
java -jar Tools\1-JBart\data\Tools\lib\apktool_2.1.1_Fluxed.jar d -f Tools\1-JBart\Settings.apk -o Tools\1-JBart\Settings\decompiled > NUL
CALL Tools\patches\patchsett.bat
CALL Tools\patches\patchsett-old.bat
ROBOCOPY Tools\patches\Settings\res Tools\1-JBart\Settings\decompiled\res /e> NUL
IF EXIST Tools\1-JBart\Settings\decompiled\apktool.yml ECHO - Settings decompiled [OK] >> log.txt
ECHO // Recompiling Settings.apk //>> log.txt
java -jar Tools\1-JBart\data\Tools\lib\apktool_2.1.1_Fluxed.jar if Tools\1-JBart\data\frameworks_single\miui.apk > NUL
java -jar Tools\1-JBart\data\Tools\lib\apktool_2.1.1_Fluxed.jar if Tools\1-JBart\data\frameworks_single\miuisystem.apk > NUL
java -jar Tools\1-JBart\data\Tools\lib\apktool_2.1.1_Fluxed.jar if Tools\1-JBart\data\frameworks_single\framework-res.apk > NUL
java -jar Tools\1-JBart\data\Tools\lib\apktool_2.1.1_Fluxed.jar if Tools\1-JBart\data\frameworks_single\framework-ext-res.apk > NUL
java -jar Tools\1-JBart\data\Tools\lib\apktool_2.1.1_Fluxed.jar b Tools\1-JBart\Settings\decompiled -o Tools\1-JBart\Settings\compiled\Settings.apk -c > NUL
XCOPY Tools\1-JBart\Settings\compiled\Settings.apk 1-Project\%PROYECTO%\3-IMG-Folder\system\priv-app\Settings /e /i /h /y >nul
CLS
ECHO ***************************************
ECHO.
IF EXIST Tools\1-JBart\Settings\compiled\Settings.apk %CECHO% - Settings.apk {0A}patched{#} & ECHO. & ECHO - Settings.apk patched>> log.txt
IF NOT EXIST Tools\1-JBart\Settings\compiled\Settings.apk %CECHO% - Settings.apk {0C}not patched{#} & ECHO. & ECHO - Settings.apk NOT patched>> log.txt
ECHO.
ECHO ***************************************
RMDIR /Q /S Tools\1-JBart\Settings
DEL Tools\1-JBart\Settings.apk
TIMEOUT /T 3 /nobreak > NUL & CLS & ECHO. >> log.txt



::Parches en services.jar y miuisystem.apk
:Parches_JBart
SET updater_script=1-Project\%PROYECTO%\1-Sources\META-INF\com\google\android\updater-script
CLS
%CECHO% {aqua on black} // Patching using JBart // {#} & ECHO. & ECHO // Patching using JBart // >> log.txt
ECHO.
START Tools\1-JBart\jbart3h.jar
ECHO  **********************************************************
ECHO.
%CECHO%  Please, use {0B}JBart{#} application for patching services.jar
ECHO.
ECHO  and disabling signature checker.
ECHO  Also patch miuisystem.apk for more RAM feature.
ECHO  **********************************************************
ECHO.
%CECHO%  JBART FOLDER PATH IS: {0B}Tools\1-JBart{#}
ECHO.
ECHO.
ECHO  Just hit "D" button for decompile each app
ECHO  in "Single File" tab.
ECHO  For Re-compile each app just hit "C" button.
ECHO.
ECHO.
ECHO  When both files are ready, just close JBart app.
ECHO  **********************************************************
ECHO.
ECHO.
%CECHO%  - HIT {0A}ENTER{#} TO CONTINUE WHEN THE APPS HAS BEEN PATCHED.
PAUSE> NUL
CLS
ECHO // Moving patched services.jar and miuisystem.apk to system // >> log.txt
XCOPY Tools\1-JBart\services.jar.bzprj\compiled\services.jar 1-Project\%PROYECTO%\3-IMG-Folder\system\framework /e /i /h /y >nul
XCOPY Tools\1-JBart\miuisystem.apk.bzprj\compiled\miuisystem.apk 1-Project\%PROYECTO%\3-IMG-Folder\system\app\miuisystem /e /i /h /y >nul
IF EXIST Tools\1-JBart\android.policy.jar.bzprj\compiled\android.policy.jar XCOPY Tools\1-JBart\services.jar.bzprj\compiled\services.jar 1-Project\%PROYECTO%\3-IMG-Folder\system\framework /e /i /h /y >nul
ECHO. >> log.txt
GOYO Writing_updater


::Identifica la ruta exacta según el dispositivo para las particiones system y boot
:Looking_for_path
CLS
%CECHO% {aqua on black} // Searching system and boot paths // {#} & ECHO. & ECHO // Searching system and boot paths // >> log.txt
ECHO.
ECHO **********************************
ECHO  -Looking for system block path...
FOR /F "Tokens=2* Delims==" %%# In (
    'TYPE "Tools\paths\%device%_path.txt" ^| FINDSTR "system.path="'
) Do (
    SET "sys_path=%%#"
)
ECHO.
FOR /F "Tokens=2* Delims==" %%# In (
    'TYPE "Tools\paths\%device%_path.txt" ^| FINDSTR "boot.path="'
) Do (
    SET "boot_path=%%#"
)
ECHO.
FOR /F "Tokens=2* Delims==" %%# In (
    'TYPE "Tools\paths\%device%_path.txt" ^| FINDSTR "data.path="'
) Do (
    SET "data_path=%%#"
)
ECHO.
ECHO -System path is: %sys_path% >> log.txt
ECHO -Boot path is: %boot_path% >> log.txt
ECHO -Data path is: %data_path% >> log.txt
CLS & ECHO. >> log.txt

:Writing_updater
CLS
%CECHO% {aqua on black} // Lets to make an updater-script // {#} & ECHO. & ECHO // Lets to make an updater-script // >> log.txt
ECHO.
ECHO.
ECHO ********************************
%CECHO%  {0A}-Writing new updater-script...{#}
ECHO.
ECHO ********************************
	if exist !updater_script! del !updater_script!
	ECHO # Auto generated by JamFlux MIUI epic kitchen >> !updater_script!
	type Tools\patches\updater-maker\upd1.txt >> !updater_script!
	!busybox! sed -i s/"midevice1/!device!/" !updater_script! | ECHO - %device% added to updater-script & ECHO - %device% added to updater-script >> log.txt
	!busybox! sed -i s/"midevice2/!device!/" !updater_script!
	ECHO ui_print(" ");>> !updater_script!
	ECHO ui_print("***********************************");>> !updater_script!
	ECHO ui_print("* Epic Patch ROM                  *");>> !updater_script!
	ECHO ui_print("***********************************");>> !updater_script!
	ECHO ui_print("* MIUI !miui! ");>> !updater_script!
	ECHO ui_print("* For !device! xiaomi device ");>> !updater_script!
	ECHO ui_print("* Running android !release! ");>> !updater_script!
	ECHO ui_print("* ROM cooked by !alias! ");>> !updater_script!
	ECHO ui_print("***********************************");>> !updater_script!
	ECHO ui_print("* Developed by JamFlux            *");>> !updater_script!
	ECHO ui_print("***********************************");>> !updater_script!
	type Tools\patches\updater-maker\%device%_upd.txt >> !updater_script!
	type Tools\patches\updater-maker\upd2.txt >> !updater_script!
	Tools\dos2unix -q !updater_script!> NUL
::Instalador número 2 para system como carpeta
	if exist !updater_script2! del !updater_script2!
	ECHO # Auto generated by JamFlux MIUI epic kitchen >> !updater_script2!
	type Tools\patches\updater-maker\upd1.txt >> !updater_script2!
	!busybox! sed -i s/"midevice1/!device!/" !updater_script2! | ECHO - %device% added to updater-script & ECHO - %device% added to updater-script2 >> log.txt
	!busybox! sed -i s/"midevice2/!device!/" !updater_script2!
	ECHO ui_print(" ");>> !updater_script2!
	ECHO ui_print("***********************************");>> !updater_script2!
	ECHO ui_print("* Epic Patch ROM                  *");>> !updater_script2!
	ECHO ui_print("***********************************");>> !updater_script2!
	ECHO ui_print("* MIUI !miui! ");>> !updater_script2!
	ECHO ui_print("* For !device! xiaomi device ");>> !updater_script2!
	ECHO ui_print("* Running android !release! ");>> !updater_script2!
	ECHO ui_print("* ROM cooked by !alias! ");>> !updater_script2!
	ECHO ui_print("***********************************");>> !updater_script2!
	ECHO ui_print("* Developed by JamFlux            *");>> !updater_script2!
	ECHO ui_print("***********************************");>> !updater_script2!
	type Tools\patches\updater-maker\%device%_upd2.txt >> !updater_script2!
	ECHO #-- SYMLINKS>> !updater_script2!
	ECHO ui_print("-- Creating symbolic links");>> !updater_script2!
	type Tools\patches\updater-maker\original_symlinks >> !updater_script2!
	type Tools\patches\updater-maker\%device%_perm.txt >> !updater_script2!
	type Tools\patches\updater-maker\upd2.txt >> !updater_script2!
	Tools\dos2unix -q !updater_script2!> NUL
	MOVE /y !updater_script2! "%~dp0"
TIMEOUT /T 5 /nobreak > NUL & CLS & ECHO. >> log.txt

::Determina el modelo del updater-script, si es en formato .dat, .img o como carpeta y es convertido a su formato original
:Re-convert
%CECHO% {aqua on black} // Converting system folder to its original system format // {#} & ECHO. & ECHO // Converting system folder to its original system format // >> log.txt
ECHO.
IF EXIST dat_format.txt goto dat_updater
IF EXIST folder_format.txt goto folder_updater
IF EXIST img_format.txt goto img_updater

:dat_updater
ECHO ************************************************
%CECHO% Your original system format is {0A}SYSTEM.NEW.DAT{#}
ECHO.
ECHO ************************************************
ECHO.
ECHO - Converting... & Echo - Converting to .dat format >> log.txt
ECHO.
call :creat_symlinks
Tools\make_ext4fs -T 0 -S 1-Project\%PROYECTO%\1-Sources\file_contexts -l %img_size% -a system 1-Project\%PROYECTO%\1-Sources\system.img 1-Project\%PROYECTO%\3-IMG-Folder\system\ >> log.txt
CLS
ECHO ************************************************
%CECHO% Your original system format is {0A}SYSTEM.NEW.DAT{#}
ECHO.
ECHO ************************************************
ECHO.
ECHO - Converting...
ECHO.
Tools\ext2simg -v 1-Project\%PROYECTO%\1-Sources\system.img 1-Project\%PROYECTO%\1-Sources\system_sparse.img >> log.txt
CLS
ECHO *********************************************
%CECHO% {0A}Choose your current zip ROM android version{#}
ECHO.
ECHO *********************************************
ECHO.
ECHO.
Tools\simg2sdat 1-Project\%PROYECTO%\1-Sources\system_sparse.img
!busybox! touch system.new.dat >> log.txt
IF EXIST system.new.dat ECHO - Converting to .dat [OK] >> log.txt
TIMEOUT /T 5 /nobreak > NUL & CLS & ECHO. >> log.txt
GOTO MOVE_ZIP



:folder_updater
ECHO ************************************************
%CECHO% Your original system format is {0A}SYSTEM FOLDER{#}
ECHO.
ECHO ************************************************
ECHO No need any convertion & ECHO -No need any convertion >> log.txt
MOVE /y 1-Project\%PROYECTO%\3-IMG-Folder\system "%~dp0"
TIMEOUT /T 5 /nobreak > NUL & CLS & ECHO. >> log.txt
GOTO MOVE_ZIP


:img_updater
ECHO ************************************************
%CECHO% Your original system format is {0A}SYSTEM.IMG{#}
ECHO.
ECHO ************************************************
ECHO.
ECHO - Converting... & Echo - Converting to .img format >> log.txt
ECHO.
call :creat_symlinks
Tools\make_ext4fs -T 0 -S 1-Project\%PROYECTO%\1-Sources\file_contexts -l %img_size% -a system 1-Project\%PROYECTO%\1-Sources\system.img 1-Project\%PROYECTO%\3-IMG-Folder\system\ >> log.txt
IF NOT EXIST 1-Project\%PROYECTO%\1-Sources\system.img goto IMG_converter4img
IF EXIST 1-Project\%PROYECTO%\1-Sources\system.img ECHO - Converting to .img [OK] >> log.txt
MOVE /y 1-Project\%PROYECTO%\1-Sources\system.img "%~dp0"
TIMEOUT /T 5 /nobreak > NUL & CLS & ECHO. >> log.txt
GOTO MOVE_ZIP




::Acá inicia la mudanza de system y de meta-inf
:MOVE_ZIP
CLS
cd %~dp0 >nul
%CECHO% {aqua on black} // Moving necessary files to zip // {#} & ECHO. & ECHO // Moving necessary files to zip // >> log.txt
ECHO.
ECHO.
ECHO *********************************************************
%CECHO% {0B}Updating{#} new files into the original zip ROM
ECHO.
%CECHO% This process can take {0A}5 minutes{#}, be patient...
ECHO.
ECHO *********************************************************
ECHO.
ECHO - Processing...
ECHO.
ECHO - Ignore some warnings... all will be OK.
MOVE /y 1-Project\%PROYECTO%\1-Sources\META-INF "%~dp0" > NUL
Tools\7za.exe a %ZIP% -o+ META-INF -y >nul >> log-7za.txt
Tools\7za.exe a %ZIP% -o+ boot.img -y >nul >> log-7za.txt
Tools\7za.exe a %ZIP% -o+ system -y >nul >> log-7za.txt
Tools\7za.exe a %ZIP% -o+ system.new.dat -y >nul >> log-7za.txt
Tools\7za.exe a %ZIP% -o+ system.patch.dat -y >nul >> log-7za.txt
Tools\7za.exe a %ZIP% -o+ system.transfer.list -y >nul >> log-7za.txt
IF NOT EXIST 1-Epic MKDIR 1-Epic > NUL >> log.txt
IF NOT EXIST 1-Epic\Modded_apps MKDIR 1-Epic\Modded_apps > NUL >> log.txt
IF NOT EXIST 1-Epic\system_installer MKDIR 1-Epic\system_installer > NUL >> log.txt
IF NOT EXIST 1-Epic\Modded_apps\frameworks MKDIR 1-Epic\Modded_apps\frameworks > NUL >> log.txt
IF EXIST 1-Project\%PROYECTO%\3-IMG-Folder\system\app\miuisystem\miuisystem.apk XCOPY 1-Project\%PROYECTO%\3-IMG-Folder\system\app\miuisystem\miuisystem.apk 1-Epic\Modded_apps\frameworks /y> NUL
IF EXIST 1-Project\%PROYECTO%\3-IMG-Folder\system\app\miui\miui.apk XCOPY 1-Project\%PROYECTO%\3-IMG-Folder\system\app\miui\miui.apk 1-Epic\Modded_apps\frameworks /y> NUL
IF EXIST 1-Project\%PROYECTO%\3-IMG-Folder\system\framework\framework-ext-res\framework-ext-res.apk XCOPY 1-Project\%PROYECTO%\3-IMG-Folder\system\framework\framework-ext-res\framework-ext-res.apk 1-Epic\Modded_apps\frameworks /y> NUL
IF EXIST 1-Project\%PROYECTO%\3-IMG-Folder\system\framework\framework-res.apk XCOPY 1-Project\%PROYECTO%\3-IMG-Folder\system\framework\framework-res.apk 1-Epic\Modded_apps\frameworks /y> NUL
IF EXIST 1-Project\%PROYECTO%\3-IMG-Folder\system\mediatek-res\mediatek-res.apk XCOPY 1-Project\%PROYECTO%\3-IMG-Folder\system\framework\mediatek-res\mediatek-res.apk 1-Epic\Modded_apps\frameworks /y> NUL
IF EXIST 1-Project\%PROYECTO%\3-IMG-Folder\system MOVE /y 1-Project\%PROYECTO%\3-IMG-Folder\system 1-Epic >> log.txt
IF EXIST system MOVE /y system 1-Epic > NUL >> log.txt
IF EXIST boot.img MOVE /y boot.img 1-Epic > NUL >> log.txt
IF EXIST META-INF\com\google\android\updater-script MOVE /y META-INF\com\google\android\updater-script 1-Epic > NUL >> log.txt
IF EXIST updater-script2 MOVE /y updater-script2 1-Epic\system_installer > NUL >> log.txt
CD 1-Epic\system_installer
RENAME updater-script2 updater-script
CD "%~dp0"
DEL system.img > NUL >> log.txt
DEL system.new.dat > NUL >> log.txt
DEL system.patch.dat > NUL >> log.txt
DEL system.transfer.list > NUL >> log.txt
IF EXIST Tools\*.pyc DEL Tools\*.pyc
IF EXIST Tools\patches\updater-maker\%device%_perms.txt DEL Tools\patches\updater-maker\%device%_perms.txt
RMDIR /Q /S META-INF > NUL >> log.txt
RMDIR /Q /S 1-Project > NUL >> log.txt
RENAME %ZIP% epic_%device%_%miui%_%release%.zip
TIMEOUT /T 5 /nobreak > NUL & CLS & ECHO. >> log.txt
GOTO Clean_JBart


:Clean_JBart
IF EXIST Tools\1-JBart\miuisystem.apk del Tools\1-JBart\miuisystem.apk > NUL
IF EXIST Tools\1-JBart\services.jar del Tools\1-JBart\services.jar > NUL
IF EXIST Tools\1-JBart\android.policy.jar del Tools\1-JBart\android.policy.jar > NUL
rmdir /Q /S Tools\1-JBart\logs
rmdir /Q /S Tools\1-JBart\miuisystem.apk.bzprj
rmdir /Q /S Tools\1-JBart\services.jar.bzprj
IF EXIST Tools\1-JBart\android.policy.jar.bzprj rmdir /Q /S Tools\1-JBart\android.policy.jar.bzprj
del /f/s/q Tools\1-JBart\data\*.apk > nul
del Tools\1-JBart\data\settings\settings.recents.conf
TIMEOUT /T 3 /nobreak > NUL & CLS


::Comprueba el resultado final
:CheckAll
CLS
%CECHO% {aqua on black} // Your new zip ROM is ready // {#} & ECHO. & ECHO // Your new zip ROM is ready // >> log.txt
ECHO.
ECHO ***********************************************
ECHO - ROM: %ZIP% & ECHO - ROM: %ZIP% >> log.txt
ECHO - Version: %release% - %miui% & ECHO - Version: %release% - %miui% >> log.txt
ECHO - Model: %device% & ECHO - Model: %device% >> log.txt
ECHO - Elapsed time: %TIME% - %TIMENOW% & ECHO - Elapsed time: %TIME% - %TIMENOW% >> log.txt
ECHO ***********************************************
TIMEOUT /T 5 /nobreak > NUL & CLS & ECHO. >> log.txt

::Comprimir log
:CompLog
CLS
%CECHO% {aqua on black} // Compressing logs // {#} & ECHO. & ECHO // Compressing logs // >> log.txt
ECHO.
ECHO ***********************************************
ECHO - Making log.7z & ECHO - Making log.7z >> log.txt
ECHO ***********************************************
ECHO.
Tools\7za a log.7z *.txt
CLS
IF EXIST log.7z %CECHO% - log.7z {0A}[OK]{#} & ECHO - log.7z [OK] >> log.txt
IF NOT EXIST log.7z ECHO - Cant make log.7z & ECHO - log.7z [OK] & PAUSE > NUL & EXIT
IF EXIST *.txt DEL *.txt
MOVE /y log.7z 1-Epic > NUL
TIMEOUT /T 5 /nobreak > NUL & CLS
exit

:admin_
>>nul 2>>&1 "%SYSTEMROOT%\system32\cacls.exe" "%SYSTEMROOT%\system32\config\system"
REM -->> If error flag set, we do not have admin.
if '%errorlevel%' NEQ '0' (
    echo.
	echo - Requesting administrator privileges...
    goto PERM
) else ( goto start )

:PERM
    echo Set UAC = CreateObject^("Shell.Application"^) >> "%temp%\admin.vbs"
    set params = %*:"=""
    echo UAC.ShellExecute "cmd.exe", "/c %~s0 %params%", "", "runas", 1 >> "%temp%\admin.vbs"

    "%temp%\admin.vbs"
    del "%temp%\admin.vbs"
    exit /B
	
:write_symlinks
	if not exist "Tools\patches\updater-maker\original_symlinks" for /f "delims=" %%a in ('Tools\find 1-Project/system -type l ^| !busybox! sed "s/1-Project//"') do (
		for /f "delims=" %%b in ('!busybox! readlink 1-Project%%a') do echo symlink("%%b", "%%a";;;| !busybox! sed "s/;;;/);/">>Tools\patches\updater-maker\last
	)
	for /f "delims=" %%a in ('echo "%cd%" ^| !busybox! cut -d":" -f1') do set drive_up=%%a
	for /f "delims=" %%a in ('echo "%cd%"^| !busybox! cut -d":" -f2') do set second=%%a
	set drive_low=!drive_up!
	for %%b in (a b c d e f g h i j k l m n o p q r s t u v w x y z) DO SET drive_low=!drive_low:%%b=%%b!
	for /f "delims=" %%a in ('echo \cygdrive\!drive_low!!second!\1-Project\system^| !busybox! tr \\ /') do set rm1=%%a
	for /f "delims=" %%a in ('echo \cygdrive\!drive_up!!second!\1-Project\system^| !busybox! tr \\ /') do set rm2=%%a
	set rm1=!rm1:/=\/!
	set rm2=!rm2:/=\/!
	set rm1=!rm1:"=!
	set rm2=!rm2:"=!
	!busybox! sed -i -e "s/!rm1!//" Tools/patches/updater-maker/last
	!busybox! sed -i -e "s/!rm2!//" Tools/patches/updater-maker/last
	set symlink_test=0
	for /f "delims=" %%a in ('!busybox! grep -cw "symlink" Tools/patches/updater-maker/last') do set symlink_test=%%a
	if exist "1-Project\system\bin\app_process64" (
		!busybox! sed -i '/^symlink("app_process32", "\/system\/bin\/app_process"/d' Tools/patches/updater-maker/last
		!busybox! sed -i '/^symlink("dalvikvm32", "\/system\/bin\/dalvikvm"/d' Tools/patches/updater-maker/last
	)
	tools\dos2unix Tools\patches\updater-maker\last >nul
	if exist "Tools\patches\updater-maker\last" !busybox! sort -u < "Tools/patches/updater-maker/last" >> "Tools/patches/updater-maker/original_symlinks"
	del Tools\patches\updater-maker\last
	Tools\find 1-Project/system -type l -delete
	goto:eof
	
:creat_symlinks
	for /f "delims=" %%a in ('type "Tools\patches\updater-maker\original_symlinks"') do (
		for /f "delims=" %%a in ('echo %%a ^| !busybox! cut -d"""" -f2') do set PLACE=%%a
		for /f "delims=" %%a in ('echo %%a ^| !busybox! cut -d"""" -f4') do set link=%%a
		for /f "delims=" %%a in ('!busybox! dirname !link!') do set linkfolder=%%a
		if not exist "1-Project/%PROYECTO%/3-IMG-Folder/!linkfolder!" mkdir "1-Project/%PROYECTO%/3-IMG-Folder/!linkfolder!"
		!busybox! ln -s -f -T !PLACE! 1-Project/%PROYECTO%/3-IMG-Folder/!link!
	)
	goto:eof

