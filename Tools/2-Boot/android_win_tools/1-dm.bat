setlocal DisableDelayedExpansion
 
set BUILDIR=ramdisk
set INTEXTFILE=fstab.qcom
 
set OUTTEXTFILE=temp.txt
set SEARCHTEXT=wait,verify
set VER=wait,
set OUTPUTLINE=
 
for /f "tokens=1,* delims=¶" %%A in ( '"type %BUILDIR%\%INTEXTFILE%"') do (
    SET string=%%A
    setLocal EnableDelayedExpansion
    SET modified=!string:%SEARCHTEXT%=%VER%!
    echo.!modified! >> %BUILDIR%\%OUTTEXTFILE%
    endlocal
 )
 
del %BUILDIR%\%INTEXTFILE%
rename %BUILDIR%\%OUTTEXTFILE% %INTEXTFILE%