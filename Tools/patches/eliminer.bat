@echo off
setlocal enabledelayedexpansion
set txtfile=original_symlinks
set newfile=symlinks
if exist "%newfile%" del /f /q "%newfile%"
set search=/WORK/system
set replace=
for /f "tokens=*" %%a in (%txtfile%) do (
   set newline=%%a
   set newline=!newline:%search%=%replace%!
   echo !newline! >> %newfile%
)