@shift /0
@ECHO OFF
COLOR 3f
SETLOCAL ENABLEDELAYEDEXPANSION
Title=Easy RAW ext4 .IMG to .DAT converter
Tools\ext2simg -v system.img system_sparse.img
CLS
ECHO --------------------------------------------
ECHO Elija la version android actual del ZIP ROM
ECHO --------------------------------------------
ECHO.
ECHO.
Tools\img2sdat.py system_sparse.img
CLS
DEL system_sparse.img
CD "%~dp0"
CD Tools
DEL blockimgdiff.pyc
DEL common.pyc
DEL rangelib.pyc
DEL sparse_img.pyc
CD "%~dp0"
DEL system.img
CLS
ECHO ---------------------
ECHO Terminado con exito
ECHO ---------------------
PAUSE>NUL
EXIT