setlocal DisableDelayedExpansion
 
set BUILDIR=Tools\1-JBart\Settings\decompiled\smali\com\android\settings\device
set INTEXTFILE=MiuiDeviceInfoSettings.smali
 
set OUTTEXTFILE=temp.txt
set SEARCHTEXT=Global 
set VER=by epic team\n
set OUTPUTLINE=
 
for /f "tokens=1,* delims=¶" %%A in ( '"type %BUILDIR%\%INTEXTFILE%"') do (
    SET string=%%A
    setLocal EnableDelayedExpansion
    SET modified=!string:%SEARCHTEXT%=%VER%!
    echo.!modified! >> %BUILDIR%\%OUTTEXTFILE%
    endlocal
 )
 
del %BUILDIR%\%INTEXTFILE%
rename %BUILDIR%\%OUTTEXTFILE% %INTEXTFILE%