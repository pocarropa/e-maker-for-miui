setlocal DisableDelayedExpansion
 
set BUILDIR=Tools\patches\updater-maker
set INTEXTFILE=original_symlinks
 
set OUTTEXTFILE=temp.txt
set SEARCHTEXT=/1-Project/system
set VER=
set OUTPUTLINE=
 
for /f "tokens=1,* delims=¶" %%A in ( '"type %BUILDIR%\%INTEXTFILE%"') do (
    SET string=%%A
    setLocal EnableDelayedExpansion
    SET modified=!string:%SEARCHTEXT%=%VER%!
    echo.!modified! >> %BUILDIR%\%OUTTEXTFILE%
    endlocal
 )
 
del %BUILDIR%\%INTEXTFILE%
rename %BUILDIR%\%OUTTEXTFILE% %INTEXTFILE%
exit