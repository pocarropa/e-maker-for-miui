mount("ext4", "EMMC", "/dev/block/bootdevice/by-name/cust", "/cust", "max_batch_time=0,commit=1,data=ordered,barrier=1,errors=panic,nodelalloc");
format("ext4", "EMMC", "/dev/block/bootdevice/by-name/system", "0", "/system");
delete("/cust/app/customized/ota-miui-MiGalleryLockscreen/ota-miui-MiGalleryLockscreen.apk",
       "/cust/app/customized/partner-XMRemoteController/partner-XMRemoteController.apk",
       "/cust/app/customized/partner-MiShop/partner-MiShop.apk",
       "/cust/app/customized/partner-BaiduSpeechService/partner-BaiduSpeechService.apk",
       "/cust/app/customized/ota-miui-MiuiForum/ota-miui-MiuiForum.apk",
       "/cust/app/customized/partner-Zixun/partner-Zixun.apk",
       "/cust/app/customized/ota-miui-XiaomiSmartHome/ota-miui-XiaomiSmartHome.apk");
delete("/cust/app/customized/recommended-3rd-com.immomo.momo.apk",
       "/cust/app/customized/recommended-3rd-com.immomo.momo/recommended-3rd-com.immomo.momo.apk",
       "/cust/app/customized/recommended-3rd-com.juanpi.ui.apk",
       "/cust/app/customized/recommended-3rd-com.juanpi.ui/recommended-3rd-com.juanpi.ui.apk",
       "/cust/app/customized/recommended-3rd-com.qiyi.video.apk",
       "/cust/app/customized/recommended-3rd-com.qiyi.video/recommended-3rd-com.qiyi.video.apk",
       "/cust/app/customized/recommended-3rd-com.qzone.apk",
       "/cust/app/customized/recommended-3rd-com.qzone/recommended-3rd-com.qzone.apk",
       "/cust/app/customized/recommended-3rd-com.sina.weibo.apk",
       "/cust/app/customized/recommended-3rd-com.sina.weibo/recommended-3rd-com.sina.weibo.apk",
       "/cust/app/customized/recommended-3rd-com.tencent.qqmusic.apk",
       "/cust/app/customized/recommended-3rd-com.tencent.qqmusic/recommended-3rd-com.tencent.qqmusic.apk",
       "/cust/app/customized/recommended-3rd-com.tuniu.app.ui.apk",
       "/cust/app/customized/recommended-3rd-com.tuniu.app.ui/recommended-3rd-com.tuniu.app.ui.apk",
       "/cust/app/customized/recommended-3rd-tv.danmaku.bili.apk",
       "/cust/app/customized/recommended-3rd-tv.danmaku.bili/recommended-3rd-tv.danmaku.bili.apk",
       "/cust/app/customized/partner-XunfeiSpeechService3.apk",
       "/cust/app/customized/partner-XunfeiSpeechService3/partner-XunfeiSpeechService3.apk");
package_extract_dir("cust", "/cust") || abort("Failed to extract dir from \"cust\" to \"/cust\".");
delete_recursive("/cust/app/customized/ota-miui-BBS_MSITE",
       "/cust/app/customized/ota-miui-GlobalMiShop",
       "/cust/app/customized/ota-miui-MiGalleryLockscreen_global",
       "/cust/app/customized/ota-partner-GooglePinyin",
       "/cust/app/customized/ota-partner-GoogleZhuyin",
       "/cust/app/customized/partner-XMRemoteController",
	   "/cust/app/customized/ota-miui-MiGalleryLockscreen",
	   "/cust/app/customized/partner-BaiduSpeechService",
	   "/cust/app/customized/partner-MiShop",
	   "/cust/app/customized/partner-Zixun",
       "/cust/app/customized/ota-miui-XiaomiSmartHome");
set_metadata_recursive("/cust", "uid", 0, "gid", 0, "dmode", 0755, "fmode", 0644, "capabilities", 0x0, "selabel", "u:object_r:system_file:s0");
show_progress(0.200000, 10);
package_extract_file("boot.img", "/dev/block/bootdevice/by-name/boot");

# ---- radio update tasks ----

ui_print("Patching firmware images...");
package_extract_file("firmware-update/cmnlib.mbn", "/dev/block/bootdevice/by-name/cmnlib");
package_extract_file("firmware-update/rpm.mbn", "/dev/block/bootdevice/by-name/rpm");
package_extract_file("firmware-update/tz.mbn", "/dev/block/bootdevice/by-name/tz");
package_extract_file("firmware-update/emmc_appsboot.mbn", "/dev/block/bootdevice/by-name/aboot");
package_extract_file("firmware-update/sbl1.mbn", "/dev/block/bootdevice/by-name/sbl1");
package_extract_file("firmware-update/keymaster.mbn", "/dev/block/bootdevice/by-name/keymaster");
package_extract_file("firmware-update/hyp.mbn", "/dev/block/bootdevice/by-name/hyp");
package_extract_file("firmware-update/cmnlib.mbn", "/dev/block/bootdevice/by-name/cmnlibbak");
package_extract_file("firmware-update/rpm.mbn", "/dev/block/bootdevice/by-name/rpmbak");
package_extract_file("firmware-update/tz.mbn", "/dev/block/bootdevice/by-name/tzbak");
package_extract_file("firmware-update/emmc_appsboot.mbn", "/dev/block/bootdevice/by-name/abootbak");
package_extract_file("firmware-update/sbl1.mbn", "/dev/block/bootdevice/by-name/sbl1bak");
package_extract_file("firmware-update/keymaster.mbn", "/dev/block/bootdevice/by-name/keymasterbak");
package_extract_file("firmware-update/hyp.mbn", "/dev/block/bootdevice/by-name/hypbak");
package_extract_file("firmware-update/NON-HLOS.bin", "/dev/block/bootdevice/by-name/modem");
package_extract_file("firmware-update/adspso.bin", "/dev/block/bootdevice/by-name/dsp");
package_extract_file("META-INF/com/miui/miui_update", "/cache/miui_update");
set_metadata("/cache/miui_update", "uid", 0, "gid", 0, "mode", 0555, "capabilities", 0x0);
run_program("/cache/miui_update");
delete("/cache/miui_update");
show_progress(0.600000, 145);
mount("ext4", "EMMC", "/dev/block/bootdevice/by-name/system", "/system");
ui_print("Extracting system...");
package_extract_dir("system", "/system");


