@echo OFF
title Simple cleaner v1.0 [by JamFlux]
SETLOCAL ENABLEDELAYEDEXPANSION

del miuisystem.apk
del services.jar
del Settings.apk
rmdir /Q /S logs
rmdir /Q /S miuisystem.apk.bzprj
rmdir /Q /S services.jar.bzprj
rmdir /Q /S Settings
del /f/s/q data\*.apk > nul
del data\settings\settings.recents.conf
exit
